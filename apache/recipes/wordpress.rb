package "php" do
	action [:install]
end
package "php-mysql" do
	action [:install]
end
package "mysql-server" do
	action [:install]
end

remote_file "/tmp/latest.tar.gz" do
        source "https://wordpress.org/latest.tar.gz"
        mode "0755"
end

bash "unzip wordpress" do
        cwd "/var/www/html"
        not_if { ::File.exists?("/var/www/html/wordpress")}
        code <<-EOH
                tar -xf /tmp/latest.tar.gz
        EOH
end

template "/etc/httpd/conf.d/wordpress.conf" do
        source "application.conf.erb"
        mode 0644
        owner "root"
        group "root"
        variables ({
                :servername => "wordpress.cignex.com",
                :DocumentRoot => "/var/www/html/wordpress"
        })
end

service "httpd" do
        action :restart
end
service "mysqld" do
        action :start
end
bash "mysql user" do
        code <<-EOH
                mysql -e "grant all on *.* to wordpress@'localhost' identified by 'wordpress';"
        EOH
end
