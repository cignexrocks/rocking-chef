# Cookbook Name:: apache
# Recipe:: default
# Copyright 2015, CIGNEX Datamatics Ltd
# All rights reserved - Do Not Redistribute

# Installing apache web server
package "httpd" do
	action [:install]
end

# Installing mod_ssl package
package "mod_ssl" do
	action [:install]
end

#Starting httpd and keeping boottime on 
service "iptables" do
	action [:disable, :stop]
end

#Starting httpd and keeping boottime on 
service "mysqld" do
	action [:enable, :start]
end

# creating apache virtual host
template "/etc/httpd/conf.d/liferay.conf" do
	source "application.conf.erb"
	mode 0644
	owner "root"
	group "root"
	variables ({
		:servername => "#{node['apache']['servername']}",
		:DocumentRoot => "#{node['apache']['DocumentRoot']}"
	})
	#notifies :relstart, 'service[httpd]', :immediately
end

# Defining liferay as a Service
cookbook_file "/etc/init.d/liferay" do
        source "liferay"
        mode "0755"
end

#Putting default index page
cookbook_file "/var/www/html/index.html" do
        source "index.html"
        mode "0755"
        owner "apache"
        group "apache"
end


# Deploy license 
#cookbook_file "/opt/liferay/deploy/license.xml" do
#        source "license.xml"
#end
bash "createlog" do
        code <<-EOH
                mkdir "/opt/liferay/tomcat-7.0.42/logs" 
		EOH
end

#Starting liferay and keeping boottime on 
service "liferay" do
        action [:enable, :start]
end

#Starting httpd and keeping boottime on 
service "httpd" do
        action [:enable, :start]
end
