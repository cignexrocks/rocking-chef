default['apache']['dir']     = "/etc/httpd"
default['apache']['sslpath']    = "/etc/httpd/ssl"
default['apache']['servername'] = "opswork.cignex.com"
default['apache']['DocumentRoot'] = "/var/www/html/"
